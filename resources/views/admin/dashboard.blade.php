@extends('admin.layout')

@section("title_page")
  Investment Report
@endsection

@section("content")
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header border-0">
              <div class="d-flex justify-content-between">
                <h3 class="card-title">Total Penjualan Tiap Bulan</h3>
              </div>
            </div>
            <div class="card-body">
              <div class="d-flex">
                <p class="d-flex flex-column">
                  <span class="text-bold text-lg">{{ $lifetime->total }} (pcs)</span>
                </p>
                <p class="ml-auto d-flex flex-column text-right">
                  <span class="text-success">
                  </span>
                </p>
              </div>
              <div class="position-relative mb-4">
                <canvas id="visitors-chart" height="200"></canvas>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Monthly Recap</h3>
            </div>
            <div class="card-body table-responsive p-0">
              <table class="table table-striped table-valign-middle">
                <thead>
                <tr>
                  <th>Month</th>
                  <th>Omzet</th>
                  <th>Quantity</th>
                  <th>Earning</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($monthly as $item)
                    <tr>
                      <td> {{ $item->new_date }} </td>
                      <td> {{ number_format($item->omzet) }} </td>
                      <td> {{ $item->pcs }} </td>
                      <td> {{ number_format($item->earning) }} </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header border-0">
              <div class="d-flex justify-content-between">
                <h3 class="card-title">Earning Percentage</h3>
              </div>
            </div>
            <div class="card-body">
              <div class="position-relative mb-4">
                <canvas id="sales-chart" height="200"></canvas>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header border-0">
              <h3 class="card-title">Online Store Overview</h3>
            </div>
            <div class="card-body">
              <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                <p class="text-success text-xl">
                  <i class="ion ion-ios-briefcase"></i>
                </p>
                <p class="d-flex flex-column text-right">
                  <span class="font-weight-bold">
                    Rp. {{ number_format($omzetMean) }}
                  </span>
                  <span class="text-muted">Rata-rata Omzet</span>
                </p>
              </div>
              <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                <p class="text-warning text-xl">
                  <i class="ion ion-ios-cart-outline"></i>
                </p>
                <p class="d-flex flex-column text-right">
                  <span class="font-weight-bold">
                      {{ $salesMean }}
                  </span>
                  <span class="text-muted">Rata-rata Penjualan</span>
                </p>
              </div>
              <div class="d-flex justify-content-between align-items-center mb-0">
                <p class="text-danger text-xl">
                  <i class="ion ion-ios-people-outline"></i>
                </p>
                <p class="d-flex flex-column text-right">
                  <span class="font-weight-bold">
                    {{ $total->cust }}
                  </span>
                  <span class="text-muted">Total Customer</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section("extrajs")
  <script>
  $(document).ready(function(){
      $(".nav-link").removeClass("active");
      $("#menuDashboard").addClass("active");
  })
  $(function () {
      'use strict'
      let salesLabel = <?php echo json_encode($label); ?>;
      let salesData = <?php echo json_encode($sales); ?>;
      let salesOmzet = <?php echo json_encode($omzet); ?>;
      let salesEarning = <?php echo json_encode($earning); ?>;
      
    var ticksStyle = {
      fontColor: '#495057',
      fontStyle: 'bold'
    }

    var mode      = 'index'
    var intersect = true

    var $salesChart = $('#sales-chart')
    var salesChart  = new Chart($salesChart, {
      type   : 'bar',
      data   : {
        labels  : salesLabel,
        datasets: [
          {
            backgroundColor: '#007bff',
            borderColor    : '#007bff',
            data           : salesEarning
          },
        ]
      },
      options: {
        maintainAspectRatio: false,
        tooltips           : {
          mode     : mode,
          intersect: intersect
        },
        hover              : {
          mode     : mode,
          intersect: intersect
        },
        legend             : {
          display: false
        },
        scales             : {
          yAxes: [{
            gridLines: {
              display      : true,
              lineWidth    : '4px',
              color        : 'rgba(0, 0, 0, .2)',
              zeroLineColor: 'transparent'
            },
            ticks    : $.extend({
              beginAtZero: true,
            }, ticksStyle)
          }],
          xAxes: [{
            display  : true,
            gridLines: {
              display: false
            },
            ticks    : ticksStyle
          }]
        }
      }
    })

    var $visitorsChart = $('#visitors-chart')
    var visitorsChart  = new Chart($visitorsChart, {
      data   : {
        labels  : salesLabel,
        datasets: [{
          type                : 'line',
          data                : salesData,
          backgroundColor     : 'transparent',
          borderColor         : '#007bff',
          pointBorderColor    : '#007bff',
          pointBackgroundColor: '#007bff',
          fill                : false
        }]
      },
      options: {
        maintainAspectRatio: false,
        tooltips           : {
          mode     : mode,
          intersect: intersect
        },
        hover              : {
          mode     : mode,
          intersect: intersect
        },
        legend             : {
          display: false
        },
        scales             : {
          yAxes: [{
          }],
          xAxes: [{
          }]
        }
      }
    })
  })

  </script>
@endsection