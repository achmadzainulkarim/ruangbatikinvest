@extends('admin.layout')

@section("title_page")
  Report
@endsection

@section("content")
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
                <div class="card-header border-0">
                    <h3 class="card-title">Sales Report</h3>
                </div>
                <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Q</th>
                            {{-- <th>Buy</th>
                            <th>Sell</th>
                            <th>Discount</th> --}}
                            <th>Earning</th>
                            <th>Feedback ({{$percent}}%)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($sales as $item)
                            <tr>
                            <td> {{ $item->date }} </td>
                            <td> {{ $item->qty }} </td>
                            {{-- <td style="text-align: right"> Rp. {{ number_format($item->buys) }} </td>
                            <td style="text-align: right"> Rp. {{ number_format($item->sells) }} </td>
                            <td style="text-align: right"> Rp. {{ number_format($item->discounts) }} </td> --}}
                            <td style="text-align: right"> Rp. {{ number_format($item->earnings) }} </td>
                            <td style="text-align: right"> Rp. {{ number_format($item->earnings * $percent / 100) }} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <div class="col-lg-6">
                <div class="card">
                        <div class="card-header border-0">
                            <h3 class="card-title">Lifetime Report</h3>
                        </div>
                        <div class="card-body table-responsive p-0">
                                <table class="table table-striped table-valign-middle">
                                    <thead>
                                    <tr>
                                        <th>Month</th>
                                        <th>Q</th>
                                        <th>Earning</th>
                                        <th>Feedback ({{$percent}}%)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($monthly as $item)
                                        <tr>
                                        <td> {{ $item->new_date }} </td>
                                        <td> {{ $item->pcs }} </td>
                                        <td style="text-align: right"> Rp. {{ number_format($item->earning) }} </td>
                                        <td style="text-align: right"> Rp. {{ number_format($item->earning * $percent / 100) }} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
          </div>
        </div>
      </div>
  </div>
@endsection

@section("extrajs")
<script>
    $(document).ready(function(){
        $(".nav-link").removeClass("active");
        $("#menuReport").addClass("active");
    })
</script>
@endsection