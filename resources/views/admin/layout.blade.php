<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Ruang Batik Investor</title>
        <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link href="{{ asset('vendor/adminlte/css/adminlte.min.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        @yield("extrastyle")
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
            </nav>
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="." class="brand-link">
                    <img src="/img/master-logo.png" class="brand-image"
                        style="opacity: .8">
                    <span class="brand-text font-weight-light">Ruang Batik</span>
                </a>
                <div class="sidebar">
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ asset('vendor/adminlte/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="/profile" class="d-block" style="text-transform: capitalize">{{ $user->name }}</a>
                        </div>
                    </div>
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <a href="./home" class="nav-link" id="menuDashboard">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="./report" class="nav-link" id="menuReport">
                                    <i class="nav-icon fas fa-paperclip"></i>
                                    <p>Report</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="logoutId">
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p>Logout</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <div class="content-wrapper">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">
                                    @yield("title_page")
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
                @yield("content")
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            
            <footer class="main-footer">
                <strong><a href="http://ruangprogram.com" target="blank">ruangprogram.com</a></strong>
                <div class="float-right d-none d-sm-inline-block">
                <b>2020</b>
                </div>
            </footer>
        </div>

        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/js/adminlte.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/js/demo.js') }}"></script>
        <script>
            $("#logoutId").click(function (){
                $("#logout-form").submit();
            })
        </script>
        @yield("extrajs")
    </body>
</html>
