@extends('admin.layout')

@section("title_page")
  Profile
@endsection

@section('extrastyle')
  <style>
    .margin10{
      margin-top: 10px;
    }
  </style>
@endsection

@section("content")
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header border-0">
                <h3 class="card-title">Profile Details</h3>
              </div>
              <div class="card-body">
                <div class="row margin10">
                  <div class="col-md-4">
                    <label>Username</label>
                  </div>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $user->name }}" disabled/>
                  </div>
                </div>
                <div class="row margin10">
                  <div class="col-md-4">
                    <label>Email</label>
                  </div>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $user->email }}" disabled/>
                  </div>
                </div>
                <div class="row margin10">
                  <div class="col-md-4">
                    <label>Date Joined</label>
                  </div>
                  <div class="col-md-8">
                    <input type="date" class="form-control" value="{{ $user->date_joined }}" disabled/>
                  </div>
                </div>
                <div class="row margin10">
                  <div class="col-md-4">
                    <label>Amount</label>
                  </div>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="Rp. {{ number_format($user->amount) }}" disabled/>
                  </div>
                </div>
                <div class="row margin10">
                  <div class="col-md-4">
                    <label>&nbsp;</label>
                  </div>
                  <div class="col-md-8">
                    <a href="#" class="btn btn-primary">
                      <i class="fas fa-pencil-alt"></i> &nbsp;
                      Edit Profile
                    </a>
                    <a href="#" class="btn btn-danger">
                      <i class="fas fa-key"></i> &nbsp;
                      Change Password
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
          </div>
        </div>
      </div>
  </div>
@endsection