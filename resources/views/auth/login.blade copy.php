<html>
    <head>
        <title>Login | Ruang Batik</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link href="{{ asset('css/login.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="login">
                <div class="heading row">
                    <img class="brand" src="/img/master-logo.png"/>
                    <span class="title">Selamat Datang</span>
                    <form action="#">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" placeholder="Username or email">
                            </div>
                    
                            <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" placeholder="Password">
                        </div>
                        <button type="submit" class="float">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>