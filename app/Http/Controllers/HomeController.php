<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $monthly = DB::select("SELECT substr(date, 1, 7) as new_date, sum(quantity) as pcs, sum(sell*quantity) as omzet, sum(((sell - buy) * quantity) - discount) as earning, sum(discount) as discount, sum(delivery) as delivery FROM `sales` left join statuses s on s.id = status_id where s.name != 'stock' GROUP by new_date order by new_date asc");
        $lifetime = DB::select("select sum(sell*quantity) as omzet, sum(quantity) as total, sum(((sell - buy) * quantity) - discount) as earning, sum(discount) as discount, sum(delivery) as delivery from sales left join statuses s on s.id = status_id where s.name != 'stock'");
        $total = DB::select("SELECT count(id) as transaksi, count(DISTINCT customer) as cust from sales");
        $omzetMean = (count($monthly) == 0) ? 0 : $lifetime[0]->omzet / count($monthly);
        $salesMean = (count($monthly) == 0) ? 0 : round($lifetime[0]->total / count($monthly));
        $label = [];
        $sales = [];
        $earning = [];
        $omzet = [];
        // dd($monthly);
        foreach($monthly as $item){
            $label[] = $item->new_date;
            $sales[] = $item->pcs;
            $omzet[] = $item->omzet;
            $earning[] = round(($item->earning / $item->omzet) * 100);
        }
        $data = [
            "label" => $label,
            "sales" => $sales,
            "omzet" => $omzet,
            "omzetMean" => $omzetMean,
            "salesMean" => $salesMean,
            "earning" => $earning,
            "lifetime" => $lifetime[0],
            "user" => $user,
            "monthly" => $monthly,
            "total" => $total[0],
        ];
        return view('admin.dashboard', $data);
    }

    public function profile(){
        $user = Auth::user();
    $userDetail = DB::select("SELECT u.name, u.email, p.date_joined, p.amount from users u left join profiles p on p.user_id = u.id where u.id = {$user->id}");
        $data = [
            "user" => $userDetail[0],
        ];
        return view('admin.profile', $data);
    }

    public function report(){
        $user = Auth::user();
        $details = DB::select("SELECT * from profiles where user_id='{$user->id}'");
        $query = "SELECT date, sum(quantity) as qty, sum(buy) as buys, sum(sell) as sells, sum(discount) as discounts, sum(earnings) as earnings from (SELECT date, quantity, buy, sell, discount, (sell-buy)*quantity-discount as earnings FROM `sales` left join statuses s on s.id = status_id where s.name != 'stock' ";
        $query .= ($details) ? "and date > '".$details[0]->date_joined."'" : "";
        $query .= ") as sale group by date";
        $sales = DB::select($query);
        $percent = ($details) ? ($details[0]->amount / 20000000) * 100 : 0;
        $query = "SELECT substr(date, 1, 7) as new_date, sum(quantity) as pcs, sum(sell*quantity) as omzet, sum(((sell - buy) * quantity) - discount) as earning, sum(discount) as discount, sum(delivery) as delivery FROM `sales` left join statuses s on s.id = status_id where s.name != 'stock' ";
        $query .= ($details) ? "and date > '".$details[0]->date_joined."'" : "";
        $query .= "GROUP by new_date order by new_date DESC";
        $monthly = DB::select($query);

        $data = [
            'user' => $user,
            "sales" => $sales,
            "percent" => $percent,
            "monthly" => $monthly,
        ]; 
        return view('admin.report', $data);
    }
}
