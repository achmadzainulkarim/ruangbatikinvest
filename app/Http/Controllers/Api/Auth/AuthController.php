<?php
namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Validator;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function postLogin(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['message' => $validator->messages()], 400);
        }

        $user = User::where('email', $request->email)->first();
        if(!$user):
            return response()->json([
                'message' => 'Email or password maybe incorrect'
            ], 401);
        endif;

        $userSession = Hash::check($request->password, $user->password);
        if(!$userSession):
            return response()->json([
                'message' => 'Email or password maybe incorrect'
            ], 401);
        endif;

        $tokenResult = $user->createToken(config('app.client_secret'));
        $token = $tokenResult->token;

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => @$user,
        ]);
    }
}